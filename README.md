# Introduction

Auteur : {{ book.author }}

Date de fabrication : {{ gitbook.time }}

Point de départ : [https://gitlab.com/pages/gitbook](https://gitlab.com/pages/gitbook)

## Ebooks

* [Support en formation PDF](/gitbook-publication-1.pdf)
* [Support en formation EPUB](/gitbook-publication-1.epub)
* [Support en formation MOBI](/gitbook-publication-1.mobi)

![Superman S symbol](https://upload.wikimedia.org/wikipedia/commons/0/05/Superman_S_symbol.svg)

Source de l'image : [Superman S symbol](https://commons.wikimedia.org/wiki/File:Superman_S_symbol.svg)
